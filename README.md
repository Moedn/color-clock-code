# Color Clock (code)

## tl;dr

C code to translate NTP time into a hue for LEDs; time is represented by that hue

## long

Basically, the program ([`color-clock-wifi-only.ino`](color-clock-wifi-only.ino)) will:

1. get the the current time via WiFi (using the Network Time Protocol (NTP))
		- default time zone is UTC+1 (e.g. Berlin/Germany)
		- winter/summer time will be adjusted automatically
2. translate this time into a specific spectral color
		- essentially, 12h are divided into 256 colors (≈2¾min per color)
		- the resulting hue is adjusted to human vision (human eyes are more sensitive to green/yellow than to red/blue (see [this chart](https://en.wikipedia.org/wiki/Color_vision#/media/File:Eyesensitivity.svg) and the [FastLED Wiki](https://github.com/FastLED/FastLED/wiki/FastLED-HSV-Colors)))
		- color transitions are "hard" (yet), there's no fading; however the steps are small enough that
3. send this color value to LEDs
4. wait 16875ms

the code has been tested with an ESP32 (DOIT ESP32 DEVKIT V1)

## ToDo before using the code on a clock

1. specify your WiFi credentials (replace `WIFI-NAME` and `WIFI-PW` directly in the code before loading it to your chip)
2. adjust the default time zone (default is UTC+1, see [eztime documentation](https://github.com/ropg/ezTime#timezones))
3. make sure your LED chipset is supported by FastLED (see the [FastLED Wiki](https://github.com/FastLED/FastLED/wiki/Chipset-reference)), e.g. WS2812 is supported
4. specify the number of your LEDs (`NUM_LEDS` default = 8)
5. adjust the default data pin, in case you need to (`DATA_PIN` default = 4)
6. in case you're using something else than typical SMD5050 LEDs, feel free to adjust the color correction (default: `setCorrection(TypicalSMD5050)`, see [FastLED documentation](https://fastled.io/docs/3.1/group___color_enums.html)); also check whether the color order matches your case (`GRB` by default as most neopixel LEDs use that)

# How To Read the Clock

256 colors get mapped onto a 12h segment of your day:

![hour-as-spectra-colour](images/hour-as-spectra-colour.png)

Here is an implementation example ↓

![implementation example](images/example.jpg)

# Attributions

## libraries used

- [WiFi.h](https://github.com/espressif/arduino-esp32/tree/master/libraries/WiFi/src)
- [eztime](https://github.com/ropg/ezTime) v0.8.3
- [FastLED.h](https://github.com/FastLED/FastLED/) (compatible to e.g. neopixel) v3.5.0

## Images

- `images/FastLED-HSV-rainbow-with-desc.jpg` from [FastLED Wiki](https://github.com/FastLED/FastLED/wiki/FastLED-HSV-Colors)
- `images/noun_clock_25218.svg` from <https://thenounproject.com/search/?q=clock&i=25218> by christoph robausch ([CC-BY-3.0](https://creativecommons.org/licenses/by/3.0/legalcode))

## Code Samples

- https://github.com/FastLED/FastLED#simple-example
- https://randomnerdtutorials.com/esp32-date-time-ntp-client-server-arduino/
- https://randomnerdtutorials.com/epoch-unix-time-esp32-arduino/
